/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from ENGINE.ai (http://xnor.ai)
 */

#ifndef ENGINE_H
#define ENGINE_H

#include <string>
#include <atomic>
#include <thread>
#include <mutex>

#include "utils.h"
#include "base.h"

namespace competer {

class Engine : public Base {
  public:
    virtual ~Engine();

  protected:
    Engine() = delete;
    Engine(unsigned int sleep_time);

  protected:
    virtual bool waitingToRun();
    virtual bool running() = 0;
    virtual bool paused();
    virtual bool waitingToHalt();

  protected:
    std::atomic<bool> on_;
    Differ diffEval_;
};

} // namespace competer

#endif // ENGINE_H
