/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from FINDPI.ai (http://xnor.ai)
 */

#include <chrono>
#include <cstring>
#include <algorithm>

#include "engine.h"

namespace competer {

Engine::Engine(unsigned int sleep_time) 
  : Base(sleep_time), on_(false) {
}

Engine::~Engine() {
}

bool Engine::waitingToRun() {
  on_ = true;
  return true;
}

bool Engine::paused() {
  return true;
}

bool Engine::waitingToHalt() {

  if (on_) {
    fprintf(stderr, "  %*.*s eval time (us): high:%u avg:%u low:%u cnt:%d\n", 
        max_name_len_, max_name_len_, getName().c_str(), diffEval_.getHigh_usec(), 
        diffEval_.getAvg_usec(), diffEval_.getLow_usec(), diffEval_.getCnt());

    on_ = false;
  }
  return true;
}

} // namespace competer

