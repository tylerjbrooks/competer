/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from FINDPI.ai (http://xnor.ai)
 */

#ifndef FINDPI_H
#define FINDPI_H

#include <string>
#include <atomic>
#include <thread>
#include <mutex>

#include "utils.h"
#include "engine.h"

namespace competer {

class FindPI : public Engine {
  public:
    static std::shared_ptr<FindPI> create(unsigned int sleep_time);
    virtual ~FindPI();

  protected:
    FindPI() = delete;
    FindPI(unsigned int sleep_time);

  protected:
    virtual bool running();
};

} // namespace competer

#endif // FINDPI_H
