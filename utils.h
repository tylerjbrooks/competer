/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from XNOR.ai (http://xnor.ai)
 */

#ifndef UTILS_H
#define UTILS_H


#include <cstdint>
#include <ctime>
#include <limits>
#include <cstring>

namespace competer {

#if defined(DEBUG_MESSAGES)
#define dbgMsg(...) fprintf(stderr,"%s %d: ",__FILE__,__LINE__); fprintf(stderr,__VA_ARGS__)
#else
#define dbgMsg(...)
#endif

class Differ {
  public:
    Differ() 
      : begin_(), end_(),
        diff_(0), diff_sum_(0), 
        cnt_(0),  avg_(0),
        high_(0), low_(std::numeric_limits<uint64_t>::max()) {
    }
    ~Differ() {}

    inline uint64_t toNano(struct timespec& ts) {
      return ts.tv_sec * (uint64_t)1000000000L + ts.tv_nsec;
    }

    inline void begin() { 
      struct timespec ts;
      clock_gettime(CLOCK_REALTIME, &ts);
      begin_ = toNano(ts);
    }

    inline void end() { 
      struct timespec ts;
      clock_gettime(CLOCK_REALTIME, &ts);
      end_ = toNano(ts);

      diff_ = end_ - begin_;
      diff_sum_ += diff_;

      high_ = (high_ < diff_) ? diff_ : high_;
      low_ = (low_ > diff_) ? diff_ : low_;

      cnt_++;
      avg_ = diff_sum_ / cnt_;
    }

    inline unsigned int getCnt()  { return cnt_; } 
    inline unsigned int getAvg_usec()  { return (unsigned int)(avg_ / 1000); }
    inline unsigned int getHigh_usec() { return (unsigned int)(high_ / 1000); }
    inline unsigned int getLow_usec()  { return (unsigned int)(low_ / 1000); }

  private:
    uint64_t begin_;
    uint64_t end_;
    uint64_t diff_;
    uint64_t diff_sum_;
    int cnt_;
    uint64_t avg_;
    uint64_t high_;
    uint64_t low_;
};

} // namespace competer

#endif // UTILS_H
