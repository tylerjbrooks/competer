# Competer

Competer is an application that runs multiple compute intensive threads that
compete for CPU time.  The user is able to control the number of threads, the
priority of the threads and the sleep time of the threads.

Xnor target detection running in single threaded mode is one of the compute
intensive tasks.  The other is a [slowly converging computation](https://en.wikipedia.org/wiki/Leibniz_formula_for_%CF%80) of PI.
On a [Raspberry Pi 3 b+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus) 
the Xnor engine takes about 250ms for each scan.  The 'FindPI' engine takes
about 100ms for each computation.  Twiddling the various realtime controls lets you
emphasize/de-emphasize the various compute intensive threads.

Competer displays a command that can be run in a separate terminal window that
shows the various running threads owned by the process (top -H -p pid).  During execution, 
the user can watch the effects of changing the thread sleep time and priorities.

Target detection is provided by the Ai2Go SDK from [XNOR](https://www.xnor.ai).

Below are instructions for building and running Competer.  

### Prerequisite

This demo requires the Ai2Go SDK from XNOR.ai.  You can sign up on
their website at [XNOR](https://www.xnor.ai).

### Installation

Get the code at:
```
git clone https://gitlab.com/tylerjbrooks/competer.git
```

### Build Notes

I build directly on the platform.  I run Raspbian from the NOOB disk on the rpiX platforms.

To build, you will need the following environment variables:
```
XNORPLATFORM=<your-platform: like 'rpi0' or 'rpi3'>
XNORMODEL=<your-model: like 'persion-pet-vehicle-detector'>
XNORVER=<your xnor sdk: like '0_3' or '0_9'>
XNORSDK=/path/to/xnor/sdk
LD_LIBRARY_PATH=/path/to/xnor/xnorver_$XNORVER/lib/$XNORPLATFORM/$XNORMODEL
```

Build competer:
```
cd competer
make
```

Use this to clean:
```
cd competer
make clean
```

### Usage

Invoke competer from the command line like this:
```
./competer -?s:f:x:w:h:

version: x.xx
where:
  ?                = this screen"                           
  (s)leeptime      = pause per loop in microseconds (default = 10000)
  (f)indpi engines = number of findpi engines       (default = 1)
  (x)nor engines   = number of xnor engines         (default = 1)
  (w)idth          = width of fake xnor image       (default = 640)
  (h)eight         = height of fake xnor image      (default = 480)
```

As an example, the following command will run two xnor engines and 
three findpi engines. It will display a 'top -H -p pid' command which you
can run in another terminal window to watch the threads work.
```
./competer -x 2 -f 3
```

Follow the instructions on the screen to learn how to change the properies.
The real time command line looks like this:
```
Change (s)leep_time             (example s:30 for 30 usec)
Change (x)nor thread priority   (example x:80 for 80 priority)
Change (f)indpi thread priority (example f:40 for 40 priority)
       (q)uit to end program

Cmd [q, s:val, x:val, or f:val]: 
```

### Discussion

Very often I work on pipelines where the sections wait for the result of
a previous section.  Most often, these sections are waiting on peripherals
(like video capture or encoding) so the CPU is free for other tasks.  However,
xnor runs unassisted and impacts the CPU.  I wanted to see what it would take
to get the xnor threads under control.

It would be helpful if xnor exposed controls that allowed the 
application engineer to control the thread priorities and sleep time.
It would also be helpful to control the number of threads that get
spawned (for multithread option).  For instance, an application developer might 
have one other compute intensive task and want to reserve a core for that task yet
still take advantage of the multithread option.

### To Do

- Add realtime controls to spin up and down new threads
