/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from XNOR.ai (http://xnor.ai)
 */

#include <chrono>
#include <csignal>
#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>

#include <sys/types.h>
#include <unistd.h>

#include "utils.h"
#include "findpi.h"
#include "xnor.h"

using namespace competer;

void usage() {
  std::cout << "competer -?s:f:x:w:h:" << std::endl;
  std::cout << "version: 0.1"   << std::endl;
  std::cout                     << std::endl;
  std::cout << "  where:"       << std::endl;
  std::cout << "  ?                = this screen" << std::endl;
  std::cout << "  (s)leeptime      = pause per loop in microseconds (default = 10000)" << std::endl;
  std::cout << "  (f)indpi engines = number of findpi engines       (default = 1)"     << std::endl;
  std::cout << "  (x)nor engines   = number of xnor engines         (default = 1)"     << std::endl;
  std::cout << "  (w)idth          = width of fake xnor image       (default = 640)"   << std::endl;
  std::cout << "  (h)eight         = height of fake xnor image      (default = 480)"   << std::endl;
}

class Competer {
  public:
    enum class Type {
      kFpi,
      kXnr
    };
  public:
    Competer() = delete;
    Competer(Competer::Type t, const char* n, std::shared_ptr<Base> e)
      : type(t), name(n), eng(e) {}
    ~Competer() {}
  public:
    Competer::Type type;
    std::string name;
    std::shared_ptr<Base> eng;
};
std::vector<Competer> engines;
Differ diffTest;

void allDone() {
  fprintf(stderr, "\n");
  fprintf(stderr, "\n");

  diffTest.end();
  fprintf(stderr, "                  test time (us): %u\n", diffTest.getAvg_usec());

  std::for_each(engines.begin(), engines.end(),
      [&](Competer const & x) { x.eng->stop(); });

  fprintf(stderr, "\n");
}

void quitHandler(int s) {
  allDone();
  exit(1);
}

void mytrim(std::string &s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
    return !std::isspace(ch);
  }));

  s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
    return !std::isspace(ch);
  }).base(), s.end());
}

int main(int argc, char** argv) {

  // defaults
  unsigned int sleep_time  = 10000;
  unsigned int fpi_engines = 1;
  unsigned int xnr_engines = 1;
  unsigned int xnr_width   = 640;
  unsigned int xnr_height  = 480;

  // cmd line options
  int c;
  while ((c = getopt(argc, argv, ":s:f:x:w:h:")) != -1) {
    switch (c) {
      case 's': sleep_time   = std::stoul(optarg); break;
      case 'f': fpi_engines = std::stoul(optarg); break;
      case 'x': xnr_engines = std::stoul(optarg); break;
      case 'w': xnr_width   = std::stoul(optarg); break;
      case 'h': xnr_height  = std::stoul(optarg); break;

      case '?':
      default:  usage(); return 0;
    }
  }

  // ctrl-c handler
  struct sigaction sig_int;
  sig_int.sa_handler = quitHandler;
  sigemptyset(&sig_int.sa_mask);
  sig_int.sa_flags = 0;
  sigaction(SIGINT, &sig_int, NULL);

  // test setup report
  fprintf(stderr, "\nTest Setup...\n");
  fprintf(stderr, "    sleep time: %d\n", sleep_time);
  fprintf(stderr, "findpi engines: %d\n", fpi_engines);
  fprintf(stderr, "  xnor engines: %d\n", xnr_engines);
  fprintf(stderr, "  xnor   width: %d\n", xnr_width);
  fprintf(stderr, "  xnor  height: %d\n", xnr_height);
  fprintf(stderr, "    process id: %d\n\n", getpid());

  // create worker threads
  for (unsigned int i = 0; i < fpi_engines; i++) {
    std::string name("findpi_");
    name += 'A' + i;
    engines.push_back(Competer(Competer::Type::kFpi, name.c_str(), 
          FindPI::create(sleep_time)));
  }
  for (unsigned int i = 0; i < xnr_engines; i++) {
    std::string name("xnor_");
    name += 'A' + i;
    engines.push_back(Competer(Competer::Type::kXnr, name.c_str(), 
          Xnor::create(sleep_time, xnr_width, xnr_height)));
  }

  // start
  dbgMsg("start\n");
  std::for_each(engines.begin(), engines.end(),
      [&](Competer const & x) { x.eng->start(x.name.c_str(), 50); });

  // run
  dbgMsg("run\n");
  std::for_each(engines.begin(), engines.end(),
      [&](Competer const & x) { x.eng->run(); });

  // test
  fprintf(stderr, "\nRun this in another window:  top -H -p %d\n\n", getpid());
  diffTest.begin();
  while (1) {

    std::string cmdstr;
    std::cout << "Change (s)leep_time             (example s:30 for 30 usec)"     << std::endl;
    std::cout << "Change (x)nor thread priority   (example x:80 for 80 priority)" << std::endl;
    std::cout << "Change (f)indpi thread priority (example f:40 for 40 priority)" << std::endl;
    std::cout << "       (q)uit to end program"                                   << std::endl;
    std::cout << std::endl;
    std::cout << "Cmd [q, s:val, x:val, or f:val]: ";
    std::getline(std::cin, cmdstr);
    std::cout << std::endl;

    mytrim(cmdstr);
    if (cmdstr.compare(0, 1, "q") == 0) {
      break;
    } else {
      std::string args;
      std::size_t colon = cmdstr.find(':');
      if (colon != std::string::npos) {
        args = cmdstr.substr(colon+1, std::string::npos);
        mytrim(args);
      }
      if (cmdstr.compare(0, 1, "s") == 0) {
        std::for_each(engines.begin(), engines.end(),
            [&](Competer const & x) { 
              x.eng->setSleepTime(std::stoul(args)); 
            });
      } else if (cmdstr.compare(0, 1, "x") == 0) {
        std::for_each(engines.begin(), engines.end(),
            [&](Competer const & x) { 
              if (x.type == Competer::Type::kXnr) { 
                x.eng->setPriority(std::stoul(args)); 
              }
            });
      } else if (cmdstr.compare(0, 1, "f") == 0) {
        std::for_each(engines.begin(), engines.end(),
            [&](Competer const & x) { 
              if (x.type == Competer::Type::kFpi) { 
                x.eng->setPriority(std::stoul(args)); 
              }
            });
      }

    }
  }

  // done
  dbgMsg("done\n");
  allDone();
  return 0;
}

