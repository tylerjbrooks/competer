# environment variables:
#   XNORPLATFORM is the xnor supported platform: like 'rpi0' or 'rpi3'
#   XNORMODEL is the xnor model: like 'person-pet-vehicle-detector'
#   XNORSDK is the location of your xnor sdk
#   LD_LIBRARAY_PATH is the loation of your xnor model library

CXX = $(RASPBIANCROSS)g++

SRC = competer.cpp base.cpp engine.cpp findpi.cpp xnor.cpp
OBJ = $(SRC:.cpp=.o)
EXE = competer

# Turn on 'DEBUG_MESSAGES' to turn on debug messages.
#FEATURES = -DDEBUG_MESSAGES

CFLAGS =-DSTANDALONE -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS -DTARGET_POSIX -D_LINUX -fPIC -DPIC -D_REENTRANT -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -U_FORTIFY_SOURCE -Wall -g -ftree-vectorize -pipe -Wno-psabi -Dxnorver_$(XNORVER) -Dxnorplat_$(XNORPLATFORM) $(FEATURES)

LDFLAGS =-L$(XNORSDK)/xnorver_$(XNORVER)/lib/$(XNORPLATFORM)/$(XNORMODEL)

LIBS =-lxnornet -lpthread -lrt -lm

INCLUDES =-I. -I$(XNORSDK)/xnorver_$(XNORVER)/include

$(EXE): $(OBJ)
	$(CXX) $(LDFLAGS) $(OBJ) $(LIBS) -o $@

.cpp.o:
	$(CXX) $(CFLAGS) $(INCLUDES) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(EXE) $(OBJ)

