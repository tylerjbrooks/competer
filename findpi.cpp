/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from FINDPI.ai (http://xnor.ai)
 */

#include <chrono>
#include <cstring>
#include <algorithm>

#include "findpi.h"

namespace competer {

FindPI::FindPI(unsigned int sleep_time) : Engine(sleep_time) {
}

FindPI::~FindPI() {
}

std::shared_ptr<FindPI> FindPI::create(unsigned int sleep_time) {
  auto obj = std::shared_ptr<FindPI>(new FindPI(sleep_time));
  return obj;
}

bool FindPI::running() {

  if (on_) {
    diffEval_.begin();

    double pi = 0.0;
    double k = 1.0;
    for (unsigned int i = 0; i < 2000000; i++, k+=2.0) {
      if (i % 2 == 0) {
        pi += 4.0 / k;
      } else {
        pi -= 4.0 / k;
      }

      if (i % 2000 == 0) {
        std::this_thread::sleep_for(std::chrono::microseconds(10));
      }
    }

    diffEval_.end();
  }
  return true;
}

} // namespace competer

