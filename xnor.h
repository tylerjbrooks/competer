/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from XNOR.ai (http://xnor.ai)
 */

#ifndef XNOR_H
#define XNOR_H

#include <string>
#include <atomic>
#include <thread>
#include <mutex>
#include <vector>

#include "utils.h"
#include "engine.h"

extern "C" {
#include "xnornet.h"
}

namespace competer {

class Xnor : public Engine {
  public:
    static std::shared_ptr<Xnor> create(unsigned int sleep_time,
        unsigned int width, unsigned int height);
    virtual ~Xnor();

  protected:
    Xnor() = delete;
    Xnor(unsigned int sleep_time);
    bool init(unsigned int width, unsigned int height);

  protected:
    virtual bool waitingToRun();
    virtual bool running();
    virtual bool waitingToHalt();

  private:
    unsigned int width_;
    unsigned int height_;
    unsigned int channels_ = {3};

    xnor_model* model_;
    unsigned int image_len_;
    std::vector<unsigned char> image_;
};

} // namespace competer

#endif // XNOR_H
