/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './competer -h' for usage.
 *
 * This program uses Ai2Go from XNOR.ai (http://xnor.ai)
 */

#include <chrono>
#include <cstring>
#include <algorithm>

#include "xnor.h"

namespace competer {

Xnor::Xnor(unsigned int sleep_time) : Engine(sleep_time) {
}

Xnor::~Xnor() {
}

std::shared_ptr<Xnor> Xnor::create(unsigned int sleep_time, 
    unsigned int width, unsigned int height) {
  auto obj = std::shared_ptr<Xnor>(new Xnor(sleep_time));
  obj->init(width, height);
  return obj;
}

bool Xnor::init(unsigned int width, unsigned int height) {

  width_ = width;
  height_ = height;

  image_len_ = width_ * height_ * channels_;
  image_.resize(image_len_);

  return true; 
}

bool Xnor::waitingToRun() {

  if (!on_) {

    // xnor options
    xnor_model_load_options* opts;
    opts = xnor_model_load_options_create();
    if (opts == nullptr) {
      dbgMsg("xnor could not create options\n");
      return false;
    }
    xnor_error* err = xnor_model_load_options_set_threading_model(opts, 
        kXnorThreadingModelSingleThreaded);
    if (err != nullptr) {
      dbgMsg("xnor could not set threading model: %s\n",
          xnor_error_get_description(err));
      return false;
    }

    // xnor model
    xnor_model* model = nullptr;
    err = xnor_model_load_built_in("", opts, &model);
    if (err != nullptr) {
      dbgMsg("xnor could not load model: %s\n",
          xnor_error_get_description(err));
      return false;
    }
    xnor_model_load_options_free(opts);

    model_ = model;

    // fake image
    unsigned char* buf = image_.data();
    srandom(time(NULL));
    for (unsigned int x = 0; x < width_; x++) {
      for (unsigned int y = 0; y < height_; y++) {
        for (unsigned int z = 0; z < channels_; z++) {
          buf[x * (height_ * channels_) + y * (channels_) + z] = 
            static_cast<unsigned char>(random());
        }
      }
    }

    on_ = true;
  }

  return true;
}

bool Xnor::running() {

  if (on_) {
    diffEval_.begin();

    // create xnor input
    xnor_input* input = nullptr;
    xnor_error* err = xnor_input_create_rgb_image(width_,
        height_, image_.data(), &input);
    if (err != nullptr) {
      dbgMsg("xnor could not create input: %s\n", 
          xnor_error_get_description(err));
      return false;
    }

    // evaluate with xnor
    xnor_evaluation_result* result = nullptr;
    err = xnor_model_evaluate(model_, input, nullptr, &result);
    if (err != nullptr) {
      dbgMsg("xnor could not evaluate: %s\n", 
          xnor_error_get_description(err));
      xnor_input_free(input);
      return false;
    }

    xnor_input_free(input);
    xnor_evaluation_result_free(result);

    diffEval_.end();
  }

  return true;
}

bool Xnor::waitingToHalt() {

  if (on_) {
    xnor_model_free(model_);
    return Engine::waitingToHalt();
  }
  return true;
}

} // namespace competer

